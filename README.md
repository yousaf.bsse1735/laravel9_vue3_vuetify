# laravel9_vue3_vuetify

## Installation

[Installation reference](https://laravel.com/docs/9.x/installation#your-first-laravel-project)
```
composer create-project laravel/laravel laravel9_vue3_vuetify

```

## Create Database
- [How to Create DB](https://hevodata.com/learn/xampp-mysql)

## Starter Kit (Breeze & Vue)

[install Laravel Breeze using Composer](https://laravel.com/docs/9.x/starter-kits#laravel-breeze-installation)
```
composer require laravel/breeze --dev

```

[install Breeze & Vue](https://laravel.com/docs/9.x/starter-kits#breeze-and-inertia)
```
php artisan breeze:install vue

php artisan migrate
npm install
npm run dev
```

## Add virtual host
- [Edit host file](https://www.howtogeek.com/27350/beginner-geek-how-to-edit-your-hosts-file/)
in folder C:\Windows\System32\drivers\etc
open file hosts
and add following
```
127.0.0.1 laravel9_vue3_vuetify.test
```

- [configure virtual host](https://www.cloudways.com/blog/configure-virtual-host-on-windows-10-for-wordpress/)
in folder C:\xampp\apache\conf\extra
open file httpd-vhosts.conf
and add following
```
<VirtualHost *:80>
DocumentRoot "c:/xampp/htdocs/laravel9_vue3_vuetify/public"
ServerName laravel9_vue3_vuetify.test
<Directory "c:/xampp/htdocs/laravel9_vue3_vuetify/public">
</Directory>
</VirtualHost>
```
## open browser and Run
For Xampp
Stop and Start Apache then 
```
http://laravel9_vue3_vuetify.test
```

## Add vuetify
- [installation](https://vuetifyjs.com/en/getting-started/installation/#manual-steps)

```
npm i vuetify

// in app.js 

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { aliases, fa } from 'vuetify/iconsets/fa'

// use vuetify
createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        const vuetify = createVuetify({
            components,
            directives,
            icons: {
                defaultSet: 'fa',
                aliases,
                sets: {
                    fa,
                }
            }
        })
        return createApp({ render: () => h(App, props) })
        .use(plugin)
        .use(vuetify)
        .use(ZiggyVue, Ziggy)
            .mount(el);
    },
    progress: {
        color: '#4B5563',
    },
});
```
